# PhPy8EG_ggH_H125_aa_4tau_test


## Setup

```
setupATLAS -3
asetup AthGeneration,21.6.55
```

## Download some LHE if not yet
```
rucio download --nrandom 1 mc15_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.evgen.TXT.e5607
```

## Run
```
Gen_tf.py --ecmEnergy=13000. --maxEvents=10 --firstEvent=1 --randomSeed=123456 --outputEVNTFile=output.root --jobConfig=9999999 --inputGeneratorFile=mc15_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.evgen.TXT.e5607/TXT.15522089._001667.tar.gz.1
```

## Run on grid
```
pathena --trf "Gen_tf.py --ecmEnergy=13000. --maxEvents=-1 --firstEvent=1 --randomSeed=%RNDM:1000 --jobConfig=9999999/ --outputEVNTFile=%OUT.root --inputGeneratorFile=%IN" --outDS=user.${USER}.`date +'%Y%m%d'`.PhPy8EG_ggH_aa_4tau_test2.evtGen.raw.root --nEventsPerJob=100 --split=50 --inDS=mc15_13TeV:mc15_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.evgen.TXT.e5607_tid10248297_00 --inTarBall=mg5gen.tgz
```
